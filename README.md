# Inspectech API Proxy

NGINX proxy app for inspectech app

## Usage

### Environment Variables

 * `LISTEN_PORT` - prot to listen on (default: `8000`)
 * `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
 * `APP_PORT` - Port of the app to forward requests to (default: `9000`)
